package com.example.demo.unit;

import com.example.demo.dao.ActivityDao;
import com.example.demo.dao.TeacherDao;
import com.example.demo.entity.Activity;
import com.example.demo.entity.Teacher;
import com.example.demo.service.ActivityServiceImpl;
import com.example.demo.service.TeacherServiceImpl;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ActivityServiceImplTest {
    @Mock
    ActivityDao activityDao;
    @InjectMocks
    ActivityServiceImpl activityService;

    static Teacher teacher1;
    static Teacher teacher2;
    static Activity activity1;
    static Activity activity2;
    @BeforeClass
    public static void setupTestData() {
        teacher1 = Teacher.builder()
                .name("Jayakrit")
                .surname("Hirisajja")
                .image(null)
                .id(1l)
                .build();
        teacher2 = Teacher.builder()
                .name("John")
                .surname("Smith")
                .image(null)
                .id(2l)
                .build();
        activity1 = Activity.builder()
                .name("CAMT Eating Contest")
                .location("CAMT 113")
                .description("Let invite CAMT students to eat")
                .host(teacher1)
                .maxEnrolledStudent(30)
                .hourCredit(10)
                .academicYear(2020)
                .semester(1)
                .id(3l)
                .build();
        activity2 = Activity.builder()
                .name("Do Nothing And Pass")
                .location("CAMT 113")
                .description("Don't do anything, just join and you will pass")
                .host(teacher2)
                .maxEnrolledStudent(1)
                .hourCredit(50)
                .academicYear(2020)
                .semester(2)
                .id(4l)
                .build();
    }

    @Before
    public void setup() {
        //Set up the injected DAO
        ArrayList<Activity> activities = new ArrayList<>();
        activities.add(activity1);
        activities.add(activity2);
        when(activityDao.getAllActivities()).thenReturn(activities);
        when(activityDao.findById(3l)).thenReturn(activity1);
        when(activityDao.findById(4l)).thenReturn(activity2);
    }

    @Test
    public void testFindingActivity() {
        //Test finding activity
        assertThat(activityService.getAllActivities().size(),is(2));
        assertThat(activityService.findById(3l),is(activity1));
        assertThat(activityService.findById(4l),is(activity2));
        assertThat(activityService.findById(3l).getHost(),is(teacher1));
    }

    @Test
    public void testDeletingActivity() {
        //Test deleting activities
        activityService.deleteById(3l);
        verify(activityDao,times(1)).deleteById(3l); //Check if student1 is removed once
        activityService.deleteById(4l);
        verify(activityDao,times(1)).deleteById(4l); //Check if student2 is removed once
    }

    @Test
    public void testAddingActivity() {
        //Set up a new activity list to test
        ArrayList<Activity> activities = new ArrayList<>();
        activities.add(activity1);
        activities.add(activity2);
        Activity activity3 = Activity.builder()
                .name("Do Not Join!")
                .location("CAMT 113")
                .description("You will not get hour credit if you join")
                .host(teacher2)
                .maxEnrolledStudent(100)
                .hourCredit(0)
                .academicYear(2020)
                .semester(1)
                .id(5l)
                .build();
        activities.add(activity3);
        when(activityDao.getAllActivities()).thenReturn(activities);
        when(activityDao.saveActivity(activity3)).thenReturn(activity3);
        //Test Adding activity
        Activity newActivity = activityService.saveActivity(activity3);
        assertThat(activityService.getAllActivities(),hasItem(newActivity));
        assertThat(activityService.getAllActivities(),hasItems(activity1,activity2,newActivity));
    }
}


