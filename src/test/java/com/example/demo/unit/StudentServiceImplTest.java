package com.example.demo.unit;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import com.example.demo.dao.StudentDao;
import com.example.demo.dao.StudentDaoImpl;
import com.example.demo.entity.Student;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.StudentService;
import com.example.demo.service.StudentServiceImpl;
import org.hamcrest.collection.IsEmptyCollection;
import org.hamcrest.collection.IsEmptyIterable;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
@RunWith(MockitoJUnitRunner.class)
public class StudentServiceImplTest {
    @Mock
    StudentDao studentDao;
    @InjectMocks
    StudentServiceImpl studentService;

    static Student student1;
    static Student student2;

    @BeforeClass
    public static void setupTestData() {
        student1 = Student.builder()
                .studentId("123456789")
                .name("Somruk")
                .surname("Laothamjinda")
                .image("notimage1.png")
                .id(1l)
                .build();
        student2 = Student.builder()
                .studentId("987654321")
                .name("Hello")
                .surname("World")
                .image("notimage2.png")
                .id(2l)
                .build();

    }

    @Before
    public void setup() {
        //Set up the injected DAO
        ArrayList<Student> students = new ArrayList<>();
        students.add(student1);
        students.add(student2);

        when(studentDao.getAllStudent()).thenReturn(students);
        when(studentDao.findById(1l)).thenReturn(student1);
        when(studentDao.findById(2l)).thenReturn(student2);
    }

    @Test
    public void testFindingStudent() {
        //Test finding students
        assertThat(studentService.getAllStudent().size(),is(2));
        assertThat(studentService.findById(1l),is(student1));
        assertThat(studentService.findById(2l),is(student2));
        assertThat(studentService.findById(3l),nullValue());
    }

    @Test
    public void testDeletingStudent() {
        //Test deleting students
        studentService.deleteById(1l);
        verify(studentDao,times(1)).deleteById(1l); //Check if student1 is removed once
        studentService.deleteById(2l);
        verify(studentDao,times(1)).deleteById(2l); //Check if student2 is removed once
    }

    @Test
    public void testAddingStudent() {
        //Set up a new student list to test
        ArrayList<Student> students = new ArrayList<>();
        students.add(student1);
        students.add(student2);
        Student student3 = Student.builder()
                .studentId("123456789")
                .name("Random")
                .surname("Dude")
                .image("notimage3.png")

                .id(3l)
                .build();
        students.add(student3);
        when(studentDao.getAllStudent()).thenReturn(students);
        when(studentDao.saveStudent(student3)).thenReturn(student3);
        //Test Adding a student
        Student newStudent = studentService.saveStudent(student3);
        assertThat(studentService.getAllStudent(),hasItem(newStudent));
        assertThat(studentService.getAllStudent(),hasItems(student1,student2,newStudent));
    }
}
