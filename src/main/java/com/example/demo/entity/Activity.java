package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Activity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @EqualsAndHashCode.Exclude
    Long id;
    String name;
    String location;
    String description;
    Integer hourCredit;
    Integer maxEnrolledStudent;
    Integer semester;
    Integer academicYear;
    @ManyToOne
    @JsonBackReference
    Teacher host;

    @ManyToMany
    @Builder.Default
    @JsonBackReference(value="enrolledStudentsReference")
    @ToString.Exclude
    List<Student> enrolledStudents = new ArrayList<>();

}
