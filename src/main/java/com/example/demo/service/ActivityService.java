package com.example.demo.service;


import com.example.demo.entity.Activity;

import java.util.List;

public interface ActivityService {
    List<Activity> getAllActivities();
    Activity findById(Long id);
    Activity deleteById(Long id);
    Activity saveActivity(Activity activity);
}
