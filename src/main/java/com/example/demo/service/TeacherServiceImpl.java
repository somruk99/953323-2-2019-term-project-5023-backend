package com.example.demo.service;

import com.example.demo.dao.TeacherDao;
import com.example.demo.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherServiceImpl implements TeacherService {
    @Autowired
    TeacherDao teacherDao;
    @Override
    public List<Teacher> getAllTeachers() {
        return teacherDao.getAllTeachers();
    }

    @Override
    public Teacher findById(Long id) {
        return teacherDao.findById(id);
    }

    @Override
    public Teacher deleteById(Long id) {
        return teacherDao.deleteById(id);
    }

    @Override
    public Teacher saveTeacher(Teacher teacher) {
        return teacherDao.saveTeacher(teacher);
    }
}
