package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentActivityDto {
    Long id;
    String studentId;
    String name;
    String surname;
    String image;
    List<ActivityDto> enrolledActivities = new ArrayList<>();
}
