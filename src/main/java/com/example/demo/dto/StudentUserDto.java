package com.example.demo.dto;

import com.example.demo.security.entity.Authority;
import com.example.demo.security.entity.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.Date;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StudentUserDto {
    Long id;
    String studentId;
    String username;
    String name;
    String surname;
    String image;
    String password;
    String email;
    Date lastPasswordResetDate;
    List<Authority> authorities;
    User user;


}
