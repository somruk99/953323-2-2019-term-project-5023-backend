package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ActivityDto {
    Long id;
    String name;
    String location;
    String description;
    Integer hourCredit;
    Integer maxEnrolledStudent;
    Integer semester;
    Integer academicYear;
    TeacherDto host;

    @Builder.Default
    List<StudentDto> enrolledStudents = new ArrayList<>();

}
