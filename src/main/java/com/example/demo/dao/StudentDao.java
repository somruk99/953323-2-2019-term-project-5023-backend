package com.example.demo.dao;


import com.example.demo.entity.Student;

import java.util.List;

public interface StudentDao {
    List<Student> getAllStudent();
    Student findById(Long id);
    Student deleteById(Long id);
    Student saveStudent(Student student);

}
