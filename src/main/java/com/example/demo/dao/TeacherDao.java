package com.example.demo.dao;

import com.example.demo.entity.Teacher;


import java.util.List;

public interface TeacherDao {
    List<Teacher> getAllTeachers();
    Teacher findById(Long id);
    Teacher deleteById(Long id);
    Teacher saveTeacher(Teacher teacher);

}
