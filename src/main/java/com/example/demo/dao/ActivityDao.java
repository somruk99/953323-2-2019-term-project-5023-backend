package com.example.demo.dao;


import com.example.demo.entity.Activity;

import java.util.List;

public interface ActivityDao {
    List<Activity> getAllActivities();
    Activity findById(Long id);
    Activity deleteById(Long id);
    Activity saveActivity(Activity activity);
}
