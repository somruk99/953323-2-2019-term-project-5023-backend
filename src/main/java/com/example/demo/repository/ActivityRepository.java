package com.example.demo.repository;

import com.example.demo.entity.Activity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ActivityRepository extends CrudRepository<Activity,Long> {
    List<Activity> findAll();
    List<Activity> findByNameIgnoreCaseContaining(String partOfName);
    List<Activity> findByAcademicYearAndSemester(Integer academicYear, Integer semester);
    List<Activity> findByEnrolledStudentsGreaterThanEqual(Integer number);
    List<Activity> findByEnrolledStudentsId(Long studentId);


}
