package com.example.demo.security.entity;

public enum AuthorityName {
    ROLE_STUDENT,ROLE_TEACHER,ROLE_ADMIN
}