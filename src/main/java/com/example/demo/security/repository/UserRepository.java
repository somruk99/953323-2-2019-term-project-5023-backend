package com.example.demo.security.repository;

import com.example.demo.security.entity.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User,Long> {
    User findByUsername(String username);
    User findByEmail(String email);
    User findByAppUserId(Long id);
    List<User> findAll();
}
