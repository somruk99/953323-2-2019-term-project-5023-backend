package com.example.demo.controller;

import com.example.demo.dto.StudentUserDto;
import com.example.demo.entity.Student;
import com.example.demo.security.entity.User;
import com.example.demo.security.repository.UserRepository;
import com.example.demo.service.StudentService;
import com.example.demo.util.MapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
public class StudentController {
    @Autowired
    StudentService studentService;
    @Autowired
    UserRepository userRepository;
    PasswordEncoder encoder = new BCryptPasswordEncoder();
    @GetMapping("/students")
    public ResponseEntity getAllStudent() {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.getAllStudent()));
    }
    @GetMapping("/students/{id}")
    public ResponseEntity getStudentById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentDto(studentService.findById(id)));
    }
    @GetMapping("/students/activities/{id}")
    public ResponseEntity getStudentActivitiesById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentActivityDto(studentService.findById(id)));
    }

    @GetMapping("/students/profile/{id}")
    public ResponseEntity getStudentProfileById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentProfileDto(studentService.findById(id)));
    }
    @CrossOrigin
    @PutMapping("/students/profile/{id}")
    public ResponseEntity<?> updateStudentProfileById(@RequestBody StudentUserDto studentUserDto, @PathVariable Long id) {
        Student student = studentService.findById(id);
        student.setImage(studentUserDto.getImage());
        student.setName(studentUserDto.getName());
        student.setSurname(studentUserDto.getSurname());
        User user = student.getUser();
        //Set new password
        user.setPassword(encoder.encode(studentUserDto.getPassword()));
        user.setFirstname(studentUserDto.getName());
        user.setLastname(studentUserDto.getSurname());
        student.setUser(user);
        user.setAppUser(student);
        studentService.saveStudent(student);
        userRepository.save(user);
        return ResponseEntity.ok(MapperUtil.INSTANCE.getStudentProfileDto(studentService.findById(id)));
    }

    @DeleteMapping("/students/{id}")
    public ResponseEntity<?> deleteStudentById(@PathVariable long id) {
        return ResponseEntity.ok(studentService.deleteById(id));
    }

    @PostMapping("/students")
    public ResponseEntity saveStudent(@RequestBody Student student) {
        return ResponseEntity.ok(studentService.saveStudent(student));
    }
}
