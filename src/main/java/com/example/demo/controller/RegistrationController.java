package com.example.demo.controller;

import com.example.demo.dto.StudentUserDto;
import com.example.demo.entity.Student;
import com.example.demo.security.entity.Authority;
import com.example.demo.security.entity.AuthorityName;
import com.example.demo.security.entity.User;
import com.example.demo.security.repository.AuthorityRepository;
import com.example.demo.security.repository.UserRepository;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Controller
public class RegistrationController {
    @Autowired
    StudentService studentService;
    @Autowired
    UserRepository userRepository;
    PasswordEncoder encoder = new BCryptPasswordEncoder();
    @Autowired
    AuthorityRepository authorityRepository;
    @CrossOrigin
    @PostMapping("/signup")
    public ResponseEntity<?> saveUser(@RequestBody StudentUserDto studentUserDto) {
        Authority studentAuthority = authorityRepository.findByName(AuthorityName.ROLE_STUDENT);
        List<Authority> authorities = new ArrayList<>();
        authorities.add(studentAuthority);
        User newUser = User.builder()
                .username(studentUserDto.getEmail())
                .firstname(studentUserDto.getName())
                .lastname(studentUserDto.getSurname())
                .password(encoder.encode(studentUserDto.getPassword()))
                .email(studentUserDto.getEmail())
                .enabled(true)
                .authorities(authorities)
                .lastPasswordResetDate(Date.from(LocalDate.of(2019, 01, 01).atStartOfDay(ZoneId.systemDefault()).toInstant()))
                .build();
        newUser = userRepository.save(newUser);
        Student newStudent = Student.builder()
                .id(newUser.getId())
                .studentId(studentUserDto.getStudentId())
                .name(studentUserDto.getName())
                .surname(studentUserDto.getSurname())
                .image(studentUserDto.getImage())
                .user(newUser)
                .build();
        newUser.setAppUser(newStudent);
        newStudent = this.studentService.saveStudent(newStudent);
        newUser = userRepository.save(newUser);
        return ResponseEntity.ok(StudentUserDto.builder()
                .id(newUser.getId())
                .username(newUser.getUsername())
                .email(newUser.getEmail())
                .studentId(newStudent.getStudentId())
                .name(newStudent.getName())
                .surname(newStudent.getSurname())
                .image(newStudent.getImage())
                .build());
    }
    @CrossOrigin
    @GetMapping("/studentUsers")
    public ResponseEntity<?> getStudentUsers() {
        List<Student> students = studentService.getAllStudent();
        List<StudentUserDto> studentUserDtoList = new ArrayList<>();
        for (Student s: students) {
            User user = s.getUser();
            StudentUserDto studentUserDto = StudentUserDto.builder()
                    .id(s.getId())
                    .studentId(s.getStudentId())
                    .email(user.getEmail())
                    .name(s.getName())
                    .surname(s.getSurname())
                    .image(s.getImage())
                    .username(user.getUsername())
                    .password(user.getPassword())
                    //Prevent overflow (More like we are lazy)
                    //.user(s.getUser())
                    .lastPasswordResetDate(user.getLastPasswordResetDate())
                    .authorities(user.getAuthorities())
                    .build();
            studentUserDtoList.add(studentUserDto);
        }
        return ResponseEntity.ok(studentUserDtoList);
    }

        @CrossOrigin
        @PostMapping("/signup/checkDupEmail")
        public ResponseEntity<?> checkDupEmail(@RequestBody StudentUserDto studentUserDto) {
            String email = studentUserDto.getEmail();
            Map<String,Boolean> result = new HashMap();

            if (userRepository.findByUsername(email) != null )
                result.put("dup",true);
            else
                result.put("dup",false);
            return ResponseEntity.ok(result);
        }
    @CrossOrigin
    @PostMapping("/signup/checkDupStudentId")
    public ResponseEntity<?> checkDupStudentId(@RequestBody StudentUserDto studentUserDto) {
        String newStudentId = studentUserDto.getStudentId();
        Map<String,Boolean> result = new HashMap();
        List<Student> students = studentService.getAllStudent();
        for (Student s : students) {
            if (s.getStudentId().equals(newStudentId)) {

                result.put("dup",true);
                return ResponseEntity.ok(result);
            }
        }
        result.put("dup",false);
        return ResponseEntity.ok(result);
    }
    @CrossOrigin
    @PostMapping("/verifyPassword/{username}/{password}")
    public ResponseEntity<?> verifyPassword(@PathVariable String username, @PathVariable String password) {
        Map<String,Boolean> result = new HashMap();
        User user = userRepository.findByUsername(username);
            if (encoder.matches(password,user.getPassword())) {
                result.put("match", true);
                return ResponseEntity.ok(result);
            }
        result.put("match",false);
        return ResponseEntity.ok(result);
    }

    }


