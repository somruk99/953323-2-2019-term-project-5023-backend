# 953323 2/2019 Term Project
## Activity Registration System (Backend)
For the frontend, please visit the below link

<https://gitlab.com/somruk99/953323-2-2019-term-project-5023-frontend>
## Features implemented

- The administrator can create activities in each semester
The activity information consists of the leader who teaches the activity (There can be one leader in each activity), the activity name and id, semester which open and the academic year, the maximum number of students who can enroll, and the number of credit of the hour attend
- The students can enrolled in the activity. The students cannot enroll more than 50 hours in each semester.
- The students can see which activity they have enrolled.
- The students can remove the activity they have enrolled in.
- The students can see how much they get the activity score. The activity hours are calculated by 2 points per hour

## Creator

Somruk Laothamjinda 602115023 

somruk_laothamjinda@elearning.cmu.ac.th

## Lecturer

Jayakrit Hirisajja

## How to run
1.) Launch an IDE which supports JAVA and Maven, personally recommends Intellij Idea since it has great Maven support.

2.) Import all the Maven modules used by the project, this will take sometime.

3.) Run the class `com.example.demo.BackendApplication` to launch the web server.

The backend was created by using Spring Boot framework and is using Java 10.